@if($orders == null)
<div class="card mb-2">
    <div class="card-body">No data found in the server</div>
</div>
@else
<div class="row">
    @foreach($orders as $order)
    <div class="col-xl-3 col-lg-4 col-md-6 mb-2">
        <div class="card mb-2">
            <div class="card-body">
                <h5 class="card-title">{{ $order->table }}</h5>
                <p class="card-text">
                    <dl>
                        <dd>Customer: {{ $order->customer }}</dd>
                        <dd>Status: {{ ($order->status) ? '<span class="text-success">Open</span>' : '<span class="text-danger">Closed</span>' }}</dd>
                    </dl>
                </p>
                <a href="#" class="btn btn-sm btn-primary">Details</a>
                <a href="#" class="btn btn-sm btn-success">Checkout</a>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif