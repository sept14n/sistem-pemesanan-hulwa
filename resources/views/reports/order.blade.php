@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3>{{ __('Order Report') }}</h3>
            <div class="card">
                <div class="card-header">
                    {{ __('Order List') }}
                    <a href="{{ route('reports.printOrder') }}" target="_blank" class="btn btn-primary ml-2">{{ __('Print') }}</a>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="lookup" width="100%" cellspacing="0">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Table</th>
                                    <th scope="col">Customer</th>
                                    <th scope="col">Bill Number</th>
                                    <th scope="col">Total Bill</th>
                                    <th scope="col">Checkout Date</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendor/datatables/css/datatables.min.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendor/datatables/js/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/autonumeric/autonumeric.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    var table = $("#lookup").dataTable({
        serverSide: true,
        lengthMenu: [[15,25,50,100], [15,25,50,100]],
        pageLength: 15,
        searching : true,
        scrollX: true,
        scrollY: '100vh',
        scrollCollapse: true,
        autoWidth: true,
        bSort: true,
        bPaginate: true,
        ajax:{
            url: "{{ url('home/reports/getOrder') }}",
            dataType: "json",
            type: "GET",
            error: function() {
                $(".lookup-error").html("");
                $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="12">No data found in the server</th></tr></tbody>');
                $("#lookup_processing").css("display","none");
            }
        },
        columns: [
            {data: 'id', width:"50px"},
            {data: 'table'},
            {data: 'customer'},
            {data: 'bill_number'},
            {data: 'total_bill'},
            {data: 'check_out_date'}
        ],
        columnDefs: [
            {
            "targets": [0],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(row+1);
                },
            },
            {
            "targets": [1],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                },
            },
            {
            "targets": [2],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                },
            },
            {
            "targets": [3],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                },
            },
            {
            "targets": [4],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).addClass('text-right currencyNoComma');
                    $(td).text(parseFloat(cellData));
                },
            },
            {
            "targets": [5],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                },
            }
        ],
        drawCallback: function(settings) {
            initAutoNumeric();
        },
        createdRow: function ( row, data, index ) {
            $(row).attr('id','table_'+index);
        }
    });
});
</script>
@endsection