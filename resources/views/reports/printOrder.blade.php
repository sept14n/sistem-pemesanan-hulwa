<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<title>Order Report - {{ config('app.name', 'Laravel') }}</title>

    <style>
    body { font-family: Arial, Helvetica, sans-serif; }
    .mb-3 { margin-bottom: 1rem; }
    .row {
        display: flex;
        flex-wrap: wrap;
        margin-right: -.75rem;
        margin-left: -.75rem;
    }
    .table {
        width: 100%;
        margin-bottom: 1rem;
        color: black;
    }
    .table-sm td, .table-sm th {
        padding: .3rem;
        vertical-align: top;
        border: 1px solid black;
    }
    .table-sm th {
        background-color: lightgrey;
    }
    .table-bordered {
        border: 1px solid black;
    }
    .text-right {
        text-align: right;
    }
    .font-weight-bold { 
        font-weight: bold; 
    }
    </style>
</head>
<body>
    @php 
        $no = 1;
    @endphp
    <center>
        <h5>Order Report</h5>
    </center>
    <dl>
        <dd>Staff: {{ Auth::user()->name }}</dd>
        <dd>Role: {{ Auth::user()->role }}</dd>
    </dl>
    <hr>
    <div class="row mb-3">
        <div style="padding: 0 .75rem">
            <h5>Order List</h5>
        </div>
    </div>
    <table class="table table-sm table-bordered" cellspacing="0">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Table</th>
                <th scope="col">Customer</th>
                <th scope="col">Bill Number</th>
                <th scope="col">Total Bill</th>
                <th scope="col">Checkout Date</th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $order['table'] }}</td>
                <td>{{ $order['customer'] }}</td>
                <td>{{ $order['bill_number'] }}</td>
                <td class="text-right">{{ $order['total_bill'] }}</td>
                <td>{{ $order['check_out_date'] }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>