@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3>{{ __('Users List') }}</h3>
            @if(session()->get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    {{ __('Registered Users') }}
                    <a href="{{ route('users.create') }}" class="btn btn-primary ml-2">{{ __('Add User') }}</a>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="lookup" width="100%" cellspacing="0">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Username</th>
                                    <th scope="col">Role</th>
                                    <th scope="col" width="140px"></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendor/datatables/css/datatables.min.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendor/datatables/js/datatables.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    var table = $("#lookup").dataTable({
        serverSide: true,
        lengthMenu: [[15,25,50,100], [15,25,50,100]],
        pageLength: 15,
        searching : true,
        scrollX: true,
        scrollY: '100vh',
        scrollCollapse: true,
        autoWidth: true,
        bSort: true,
        bPaginate: true,
        ajax:{
            url: "{{ url('home/users/getIndex') }}",
            dataType: "json",
            type: "GET",
            error: function() {
                $(".lookup-error").html("");
                $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="12">No data found in the server</th></tr></tbody>');
                $("#lookup_processing").css("display","none");
            }
        },
        columns: [
            {data: 'id', width:"50px"},
            {data: 'name'},
            {data: 'username'},
            {data: 'role'},
            {data: 'id', width:"140px"}
        ],
        columnDefs: [
            {
            "targets": [0],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(row+1);
                },
            },
            {
            "targets": [1],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                },
            },
            {
            "targets": [2],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                },
            },
            {
            "targets": [3],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                },
            },
            {
            "targets": [4],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append('<div class="btn-group" role="group" aria-label="Basic example"><a href="users/' + cellData + '/edit" class="btn btn-primary btn-edit-record"><i class="fas fa-fw fa-edit"></i></a><a href="users/' + cellData + '/delete" class="btn btn-danger btn-del-record"><i class="fas fa-fw fa-trash"></i></a></div>');
                    $(td).addClass('text-center');
                },
            }
        ],
        createdRow: function ( row, data, index ) {
            $(row).attr('id','table_'+index);
        }
    });

    $('.table').on('click', '.btn-edit-record', function() {
        var tr = $(this).closest('tr');
        var row = table.api().row( tr );
        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData();

        location.href = "{!! url('home/users') !!}/" + data[index].id + "/edit";
    });

    $('.table').on('click', '.btn-del-record', function() {
        var tr = $(this).closest('tr');
        var row = table.api().row( tr );
        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData();

        $.ajax({
            url: "{!! url('home/users') !!}/" + data[index].id,
            type: "DELETE",
        });
    });

});
</script>
@endsection