@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3>{{ __('Add Menu') }}</h3>
            <div class="card">
                <div class="card-header">{{ __('Menu Profile') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('menus.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="desc" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                            <div class="col-md-6">
                                <input id="desc" type="text" class="form-control @error('desc') is-invalid @enderror" name="desc" value="{{ old('desc') }}" required autocomplete="desc">

                                @error('desc')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">{{ __('Type') }}</label>

                            <div class="col-md-6">
                                <div class="form-check form-check-inline mt-2">
                                    <input class="form-check-input @error('type') is-invalid @enderror" type="radio" name="type" id="type1" value="Food" checked required>
                                    <label class="form-check-label" for="type1">Food</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input @error('type') is-invalid @enderror" type="radio" name="type" id="type2" value="Beverage">
                                    <label class="form-check-label" for="type2">Beverage</label>
                                </div>

                                @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>

                            <div class="col-md-6">
                                <input id="price" type="text" class="form-control @error('price') is-invalid @enderror currencyNoComma" name="price" value="{{ old('price') }}" required autocomplete="price">

                                @error('price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ready" class="col-md-4 col-form-label text-md-right">{{ __('Ready') }}</label>

                            <div class="col-md-6">
                                <div class="form-check form-check-inline mt-2">
                                    <input class="form-check-input @error('ready') is-invalid @enderror" type="radio" name="ready" id="ready1" value="1" checked required>
                                    <label class="form-check-label" for="ready1">Ready</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input @error('ready') is-invalid @enderror" type="radio" name="ready" id="ready2" value="0">
                                    <label class="form-check-label" for="ready2">Not Ready</label>
                                </div>

                                @error('ready')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                                <a href="{{ route('menus.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('vendor/autonumeric/autonumeric.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    initAutoNumeric();
});
</script>
@endsection