@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3>{{ __('Menus List') }}</h3>
            @if(session()->get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    {{ __('Registered Menus') }}
                    <a href="{{ route('menus.create') }}" class="btn btn-primary ml-2">{{ __('Add Menu') }}</a>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="lookup" width="100%" cellspacing="0">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Ready</th>
                                    <th scope="col" width="140px"></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendor/datatables/css/datatables.min.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendor/datatables/js/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/autonumeric/autonumeric.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    var table = $("#lookup").dataTable({
        serverSide: true,
        lengthMenu: [[15,25,50,100], [15,25,50,100]],
        pageLength: 15,
        searching : true,
        scrollX: true,
        scrollY: '100vh',
        scrollCollapse: true,
        autoWidth: true,
        bSort: true,
        bPaginate: true,
        ajax:{
            url: "{{ url('home/menus/getIndex') }}",
            dataType: "json",
            type: "GET",
            error: function() {
                $(".lookup-error").html("");
                $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="12">No data found in the server</th></tr></tbody>');
                $("#lookup_processing").css("display","none");
            }
        },
        columns: [
            {data: 'id', width:"50px"},
            {data: 'name'},
            {data: 'desc'},
            {data: 'type'},
            {data: 'price'},
            {data: 'ready'},
            {data: 'id', width:"140px"}
        ],
        columnDefs: [
            {
            "targets": [0],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(row+1);
                },
            },
            {
            "targets": [1],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                },
            },
            {
            "targets": [2],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                },
            },
            {
            "targets": [3],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                },
            },
            {
            "targets": [4],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).addClass('text-right currencyNoComma');
                    $(td).text(parseFloat(cellData));
                },
            },
            {
            "targets": [5],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    if(cellData) {
                        $(td).append('<span class="text-success">Ready</span>');
                    } else {
                        $(td).append('<span class="text-danger">Not Ready</span>');
                    }
                },
            },
            {
            "targets": [6],
            "data": null,
            "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).append('<div class="btn-group" role="group" aria-label="Basic example"><a href="menus/' + cellData + '/edit" class="btn btn-primary btn-edit-record"><i class="fas fa-fw fa-edit"></i></a><a href="menus/' + cellData + '/delete" class="btn btn-danger btn-del-record"><i class="fas fa-fw fa-trash"></i></a></div>');
                    $(td).addClass('text-center');
                },
            }
        ],
        drawCallback: function(settings) {
            initAutoNumeric();
        },
        createdRow: function ( row, data, index ) {
            $(row).attr('id','table_'+index);
        }
    });
});
</script>
@endsection