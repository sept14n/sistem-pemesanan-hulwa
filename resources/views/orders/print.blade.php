<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<title>Print Order - {{ config('app.name', 'Laravel') }}</title>

    <style>
    body { font-family: Arial, Helvetica, sans-serif; }
    .mb-3 { margin-bottom: 1rem; }
    .row {
        display: flex;
        flex-wrap: wrap;
        margin-right: -.75rem;
        margin-left: -.75rem;
    }
    .table {
        width: 100%;
        margin-bottom: 1rem;
        color: black;
    }
    .table-sm td, .table-sm th {
        padding: .3rem;
        vertical-align: top;
        border: 1px solid black;
    }
    .table-sm th {
        background-color: lightgrey;
    }
    .table-bordered {
        border: 1px solid black;
    }
    .text-right {
        text-align: right;
    }
    .font-weight-bold { 
        font-weight: bold; 
    }
    </style>
</head>
<body>
    @php 
        $no = 1; 
        $total = 0; 
        foreach($details as $detail) {
            $total = $detail['total'];
        }
    @endphp
    <center>
        <h5>Print Order {{ $order->tables->name }}</h5>
    </center>
    <dl>
        <dd>Customer: {{ $order->customer }}</dd>
        <dd>Total Bill: <span class="font-weight-bold">{{ $total }}</span></dd>
        <dd>Bill Number: {{ $order->bill_number }}</dd>
    </dl>
    <hr>
    <div class="row mb-3">
        <div style="padding: 0 .75rem">
            <h5>Order Items</h5>
        </div>
    </div>
    <table class="table table-sm table-bordered" cellspacing="0">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Menu</th>
                <th scope="col">Type</th>
                <th scope="col">Qty</th>
                <th scope="col">Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach($details as $detail)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $detail['menu'] }}</td>
                <td>{{ $detail['type'] }}</td>
                <td class="text-right">{{ $detail['qty'] }}</td>
                <td class="text-right font-weight-bold">{{ $detail['total'] }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>