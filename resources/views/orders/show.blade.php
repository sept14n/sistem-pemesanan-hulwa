@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3>{{ __('Order Details') }}</h3>
            @if(session()->get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    {{ __('Registered Order') }}
                    <div class="btn-group" role="group" aria-label="Basic example">
                        @if($order->status)
                            @if(Auth::user()->role == 'cashier')
                            <a href="{{ route('orders.checkout', $order->id) }}" class="btn btn-success">{{ __('Checkout') }}</a>
                            @endif
                        @else
                        <a href="{{ route('orders.printOrder', $order->id) }}" target="_blank" class="btn btn-primary">{{ __('Print') }}</a>
                        @endif
                        <a href="{{ route('orders.index') }}" class="btn btn-secondary">{{ __('Back') }}</a>
                    </div>
                </div>

                <div class="card-body">
                    <h5 class="card-title">
                        {{ $order->tables->name }}
                        @if($order->status)
                        <span class="badge badge-info">Open</span>
                        @else
                        <span class="badge badge-danger">Closed</span>
                        @endif
                    </h5>
                    <p class="card-text">
                        <dl>
                            <dd>Customer: {{ $order->customer }}</dd>
                            <dd>Total Bill: <span id="total_bill" class="font-weight-bold"></span></dd>
                            @if(!$order->status)
                            <dd>Bill Number: {{ $order->bill_number }}</dd>
                            @endif
                        </dl>
                        <hr>
                        <div class="row mb-3">
                            <div style="padding: 0 .75rem">
                                <h5>Order Items</h5>
                            </div>
                            @if($order->status)
                            <div style="padding: 0 .75rem">
                                <a href="{{ route('orders.createDetail', $order->id) }}" class="btn btn-sm btn-primary">{{ __('Add Item') }}</a>
                            </div>
                            @endif
                        </div>
                        <div class="table-responsive">
                            <table class="table table-sm table-bordered" id="lookup" width="100%" cellspacing="0">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Menu</th>
                                        <th scope="col">Type</th>
                                        <th scope="col">Qty</th>
                                        <th scope="col">Total</th>
                                        @if($order->status)
                                        <th scope="col" width="75px"></th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $no = 1; $total = 0; @endphp
                                    @if(!empty($details))
                                    @foreach($details as $detail)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $detail['menu'] }}</td>
                                        <td>{{ $detail['type'] }}</td>
                                        <td class="text-right">{{ $detail['qty'] }}</td>
                                        <td class="text-right font-weight-bold currencyNoComma">{{ $detail['total'] }}</td>
                                        @if($order->status)
                                        <td class="text-center">
                                            <a href="{{ route('orders.deleteDetail', [$order->id, $detail['id']]) }}" class="btn btn-sm btn-danger btn-del-record"><i class="fas fa-fw fa-trash"></i></a>
                                        </td>
                                        @endif
                                    </tr>
                                    @php $total += $detail['total']; @endphp
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('vendor/autonumeric/autonumeric.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    initAutoNumeric();

    $('#total_bill').text(thousand_separator({{ $total }}));

    function thousand_separator(n) {
        if(typeof n === 'number'){
            n += '';
            var x = n.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        } else {
            return n;
        }
    }
});
</script>
@endsection