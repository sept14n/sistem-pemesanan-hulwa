@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3>{{ __('Add Order') }}</h3>
            <div class="card">
                <div class="card-header">{{ __('Order Profile') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('orders.store') }}">
                        @csrf

                        <input type="hidden" name="check_in_date" id="check_in_date">

                        <div class="form-group row">
                            <label for="customer" class="col-md-4 col-form-label text-md-right">{{ __('Customer Name') }}</label>

                            <div class="col-md-6">
                                <input id="customer" type="text" class="form-control @error('customer') is-invalid @enderror" name="customer" value="{{ old('customer') }}" required autocomplete="customer" autofocus>

                                @error('customer')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="table_id" class="col-md-4 col-form-label text-md-right">{{ __('Table') }}</label>

                            <div class="col-md-6">
                                <select name="table_id" id="table_id" class="form-control @error('table_id') is-invalid @enderror" required>
                                </select>

                                @error('table_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                                <a href="{{ route('orders.index') }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    var dt = new Date();
    var date = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate()
    var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
    $('#check_in_date').val(date + " " + time)    

    $.ajax({
        url: "{{ route('orders.getTable') }}",
        type: "GET",
        success: function (mydata) {
            $.each(eval(mydata), function(key, value) {
                $('#table_id').append($('<option></option>')
                                        .attr('value', value.id)
                                        .text(value.name));
            })
        }
    });
});
</script>
@endsection