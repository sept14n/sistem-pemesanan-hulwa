@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3>{{ __('Add Item') }}</h3>
            <div class="card">
                <div class="card-header">{{ __('Order Item') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('orders.storeDetail', $order->id) }}">
                        @csrf
                        
                        <input type="hidden" name="order_id" id="order_id" value="{{ $order->id }}">

                        <div class="form-group row">
                            <label for="menu_id" class="col-md-4 col-form-label text-md-right">{{ __('Item') }}</label>

                            <div class="col-md-6">
                                <input type="hidden" name="price" id="price">
                                <select name="menu_id" id="menu_id" class="form-control @error('menu_id') is-invalid @enderror" required autofocus>
                                </select>

                                @error('menu_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="qty" class="col-md-4 col-form-label text-md-right">{{ __('Quantity') }}</label>

                            <div class="col-md-6">
                                <input id="qty" type="text" class="form-control @error('qty') is-invalid @enderror currencyNoComma" name="qty" value="{{ old('qty') }}" required autocomplete="qty">

                                @error('qty')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                                <a href="{{ route('orders.show', $order->id) }}" class="btn btn-secondary">{{ __('Cancel') }}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('vendor/autonumeric/autonumeric.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    initAutoNumeric();

    var arPrice = [];
    
    $('#menu_id').on('change', function() {
        $('#price').val(arPrice[$(this).prop('selectedIndex')]);
    });    

    $.ajax({
        url: "{{ route('orders.getMenu') }}",
        type: "GET",
        success: function (mydata) {
            $.each(eval(mydata), function(key, value) {
                $('#menu_id').append($('<option></option>')
                                        .attr('value', value.id)
                                        .text(value.name));
                
                arPrice.push(value.price);
            });
        },
        complete: function (mydata) {
            $('#price').val(arPrice[$('#menu_id').prop('selectedIndex')]);
        }
    });
});
</script>
@endsection