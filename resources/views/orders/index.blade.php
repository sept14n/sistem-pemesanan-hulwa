@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3>{{ __('Orders List') }}</h3>
            @if(session()->get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session()->get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    {{ __('Registered Orders') }}
                    <a href="{{ route('orders.create') }}" class="btn btn-primary ml-2">{{ __('Add Order') }}</a>
                </div>

                <div class="card-body">
                    @if($orders == null)
                    <div class="card mb-2">
                        <div class="card-body text-center">No data found in the server</div>
                    </div>
                    @else
                    <div class="row">
                        @foreach($orders as $order)
                        <div class="col-xl-3 col-lg-4 col-md-6 mb-2">
                            <div class="card mb-2">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $order['table'] }}</h5>
                                    <p class="card-text">
                                        <dl>
                                            <dd>Customer: {{ $order['customer'] }}</dd>
                                            @if($order['status'])
                                            <dd>Status: <span class="text-info">Open</span></dd>
                                            @else
                                            <dd>Status: <span class="text-danger">Closed</span></dd>
                                            @endif
                                        </dl>
                                    </p>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="{{ route('orders.show', $order['id']) }}" class="btn btn-sm btn-primary">Details</a>
                                        @if($order['status'])
                                            @if(Auth::user()->role == 'cashier')
                                            <a href="{{ route('orders.checkout', $order['id']) }}" class="btn btn-sm btn-success">Checkout</a>
                                            @endif
                                        @else
                                            @if(Auth::user()->id == $order['user_id'] || Auth::user()->role == 'cashier')
                                            <a href="{{ route('orders.printOrder', $order['id']) }}" target="_blank" class="btn btn-sm btn-outline-primary">{{ __('Print') }}</a>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {

});
</script>
@endsection