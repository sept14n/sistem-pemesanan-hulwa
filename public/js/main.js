function initAutoNumeric(){
    $('.percent').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            new AutoNumeric(this,{
                currencySymbol: "%",
                currencySymbolPlacement: "s",
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalPlaces: 2,
                unformatOnSubmit: true,
                watchExternalChanges: true
            });
        }
    });
    $('.percentReadOnly').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            new AutoNumeric(this,{
                currencySymbol: "%",
                currencySymbolPlacement: "s",
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalPlaces: 2,
                noEventListeners: true,
                readOnly: true,
                unformatOnSubmit: true,
                watchExternalChanges: true
            });
        }
    });
    $('.percentNoComma').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            new AutoNumeric(this,{
                currencySymbol: "%",
                currencySymbolPlacement: "s",
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalPlaces: 0,
                unformatOnSubmit: true,
                watchExternalChanges: true
            });
        }
    });
    $('.percentComma').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            new AutoNumeric(this,{
                currencySymbol: "%",
                currencySymbolPlacement: "s",
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalPlaces: 5,
                unformatOnSubmit: true,
                watchExternalChanges: true
            });
        }
    });
    $('.percentNoCommaReadOnly').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            new AutoNumeric(this,{
                currencySymbol: "%",
                currencySymbolPlacement: "s",
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalPlaces: 0,
                noEventListeners: true,
                readOnly: true,
                unformatOnSubmit: true,
                watchExternalChanges: true
            });
        }
    });
    $('.currency').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            new AutoNumeric(this,{
                currencySymbol: "",
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalPlaces: 2,
                maximumValue:1000000000000000000,
                unformatOnSubmit: true,
                watchExternalChanges: true
            });
        }
    });
    $('.currencyNoComma').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            if($(this).is('input:not([readonly])')){
                new AutoNumeric(this,{
                    currencySymbol: "",
                    digitGroupSeparator: ',',
                    decimalCharacter: '.',
                    decimalPlaces: 0,
                    unformatOnSubmit: true,
                    watchExternalChanges: true,
                    maximumValue:999999,
                    minimumValue:0,
                    autoNumeric:'formatted'
                });
            }else{
                new AutoNumeric(this,{
                    currencySymbol: "",
                    digitGroupSeparator: ',',
                    decimalCharacter: '.',
                    decimalPlaces: 0,
                    maximumValue:999999,
                    unformatOnSubmit: true,
                    watchExternalChanges: true,
                    autoNumeric:'formatted'
                });
            }
        }
    });
    $('.currencyNoCommaReadOnly').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            new AutoNumeric(this,{
                currencySymbol: "",
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalPlaces: 0,
                noEventListeners: true,
                readOnly: true,
                maximumValue:1000000000000000000,
                unformatOnSubmit: true,
                watchExternalChanges: true
            });
        }
    });
}