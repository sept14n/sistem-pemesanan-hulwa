<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = [
        'order_id', 'menu_id', 'qty',
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function menus()
    {
        return $this->hasOne('App\Menu', 'id', 'menu_id');
    }
}
