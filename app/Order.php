<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'customer', 'table_id', 'check_in_date', 'bill_number', 'total_bill', 'status', 'check_out_date', 'user_id',
    ];

    public function tables()
    {
        return $this->belongsTo('App\Table', 'table_id');
    }

    public function order_details()
    {
        return $this->hasMany('App\OrderDetail');
    }
}
