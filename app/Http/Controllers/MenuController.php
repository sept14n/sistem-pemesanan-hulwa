<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('menus.index');
    }

    public function getIndex()
    {
        $menus = Menu::all();
        
        return Datatables::of($menus)->escapeColumns([])->make(true);
    }

    public function getIndexAPI()
    {
        $data = Menu::all();
        
        if (count($data) > 0) {
            $res['message'] = "Success!";
            $res['data'] = $data;
            return response($res);
        }
        else {
            $res['message'] = "Empty!";
            return response($res);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string', 'max:255'],
            'type' => ['required', 'string', 'max:255'],
            'price' => ['required', 'numeric', 'min:1', 'max:999999.99'],
            'ready' => ['required'],
        ]);

        $menu = new Menu([
            'name' => $request['name'],
            'desc' => $request['desc'],
            'type' => $request['type'],
            'price' => $request['price'],
            'ready' => $request['ready'],
        ]);
        $menu->save();

        return redirect('/home/menus')->with('success', 'Menu has been submited.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::find($id);

        return view('menus.edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string', 'max:255'],
            'type' => ['required', 'string', 'max:255'],
            'price' => ['required', 'numeric', 'min:1', 'max:999999.99'],
            'ready' => ['required'],
        ]);

        $menu = Menu::find($id);
        $menu->name = $request->get('name');
        $menu->desc = $request->get('desc');
        $menu->type = $request->get('type');
        $menu->price = $request->get('price');
        $menu->ready = $request->get('ready');
        $menu->save();

        return redirect('/home/menus')->with('success', 'Menu has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);
        $menu->delete();

        return redirect('/home/menus')->with('success', 'Menu has been deleted.');
    }
}
