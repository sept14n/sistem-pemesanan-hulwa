<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use Yajra\Datatables\Datatables;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function order()
    {
        return view('reports.order');
    }

    public function getOrder()
    {
        if (Auth::user()->role != 'admin') {
            $order = Order::where([
                    ['user_id', '=', Auth::user()->id],
                    ['status', '=', 0]
                ])->get();
        } else {
            $order = Order::where('status', 0)->get();
        }

        $orders = [];
        foreach ($order as $key => $value) {
            $orders[$key] = array(
                'id' => $value->id,
                'table' => $value->tables->name,
                'customer' => $value->customer,
                'bill_number' => $value->bill_number,
                'total_bill' => $value->total_bill,
                'check_out_date' => $value->check_out_date,
            );
        }

        return Datatables::of($orders)->escapeColumns([])->make(true);
    }

    public function printOrder()
    {
        if (Auth::user()->role != 'admin') {
            $order = Order::where([
                    ['user_id', '=', Auth::user()->id],
                    ['status', '=', 0]
                ])->get();
        } else {
            $order = Order::where('status', 0)->get();
        }

        $orders = [];
        foreach ($order as $key => $value) {
            $orders[$key] = array(
                'id' => $value->id,
                'table' => $value->tables->name,
                'customer' => $value->customer,
                'bill_number' => $value->bill_number,
                'total_bill' => $value->total_bill,
                'check_out_date' => $value->check_out_date,
            );
        }

        $pdf = PDF::loadView('reports.printOrder', compact('orders'))->setPaper('A4', 'potrait');
        return $pdf->stream();
    }
}
