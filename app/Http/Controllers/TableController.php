<?php

namespace App\Http\Controllers;

use App\Table;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tables.index');
    }

    public function getIndex()
    {
        $tables = Table::all();
        
        return Datatables::of($tables)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tables.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string', 'max:255'],
            'active' => ['required'],
        ]);

        $table = new Table([
            'name' => $request['name'],
            'desc' => $request['desc'],
            'active' => $request['active'],
        ]);
        $table->save();

        return redirect('/home/tables')->with('success', 'Table has been submited.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $table = Table::find($id);

        return view('tables.edit', compact('table'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string', 'max:255'],
            'active' => ['required'],
        ]);

        $table = Table::find($id);
        $table->name = $request->get('name');
        $table->desc = $request->get('desc');
        $table->active = $request->get('active');
        $table->save();

        return redirect('/home/tables')->with('success', 'Table has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $table = Table::find($id);
        $table->delete();

        return redirect('/home/tables')->with('success', 'Table has been deleted.');
    }
}
