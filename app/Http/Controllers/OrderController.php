<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Table;
use App\Order;
use App\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use Yajra\Datatables\Datatables;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 'waiter') {
            $order = Order::where('user_id', Auth::user()->id)->get();
        } else {
            $order = Order::all();
        }

        $orders = [];
        foreach ($order as $key => $value) {
            $orders[$key] = array(
                'id' => $value->id,
                'table' => $value->tables->name,
                'customer' => $value->customer,
                'status' => $value->status,
                'user_id' => $value->user_id,
            );
        }

        return view('orders.index', compact('orders'));
    }

    public function getTable()
    {
        $table = Table::select('id', 'name')
                    ->where('active', 1)
                    ->orderBy('name', 'asc')
                    ->get();
        
        return json_encode($table);
    }

    public function getMenu()
    {
        $table = Menu::select('id', 'name', 'price')
                    ->where('ready', 1)
                    ->orderBy('type', 'desc')
                    ->get();
        
        return json_encode($table);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'customer' => ['required', 'string', 'max:255'],
            'table_id' => ['required'],
        ]);

        $order = new Order([
            'customer' => $request['customer'],
            'table_id' => $request['table_id'],
            'check_in_date' => date('Y-m-d H:i:s'),
            'bill_number' => "-",
            'total_bill' => 0,
            'status' => true,
            'user_id' => Auth::user()->id,
        ]);
        $order->save();

        $last = Order::latest()->first();

        return redirect('/home/orders/' . $last->id)->with('success', 'Order has been submited.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $detail = OrderDetail::where('order_id', $id)->get();
        $details = [];
        foreach ($detail as $key => $value) {
            $menu = Menu::where('id', $value->menu_id)->get();
            $details[$key] = array(
                "id" => $value->id,
                "menu" => $menu[0]->name,
                "type" => $menu[0]->type,
                "qty" => $value->qty,
                "total" => $menu[0]->price * $value->qty,
            );
        }

        return view('orders.show', compact('order', 'details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);

        return view('orders.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'customer' => ['required', 'string', 'max:255'],
            'table_id' => ['required'],
        ]);

        $order = Order::find($id);
        $order->customer = $request->get('customer');
        $order->table_id = $request->get('table_id');
        $order->check_in_date = date('Y-m-d H:i:s');
        $order->save();

        return redirect('/home/orders')->with('success', 'Order has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();

        return redirect('/home/orders')->with('success', 'Order has been deleted.');
    }

    public function createDetail($id)
    {
        $order = Order::find($id);

        return view('orders.createDetail', compact('order'));
    }

    public function storeDetail($id, Request $request)
    {
        $request->validate([
            'order_id' => ['required'],
            'menu_id' => ['required'],
            'qty' => ['required'],
        ]);

        $order = new OrderDetail([
            'order_id' => $request['order_id'],
            'menu_id' => $request['menu_id'],
            'qty' => $request['qty'],
        ]);
        $order->save();

        return redirect('/home/orders/' . $id)->with('success', 'Order Item has been submited.');
    }
    
    public function deleteDetail($id, $idDetail)
    {
        $detail = OrderDetail::find($idDetail);
        $detail->delete();

        return redirect('/home/orders/' . $id)->with('success', 'Order Item has been deleted.');
    }

    public function checkout($id)
    {
        $order = Order::find($id);
        $detail = OrderDetail::where('order_id', $id)->get();
        $details = [];
        foreach ($detail as $key => $value) {
            $menu = Menu::where('id', $value->menu_id)->get();
            $details[$key] = array(
                "id" => $value->id,
                "menu" => $menu[0]->name,
                "type" => $menu[0]->type,
                "qty" => $value->qty,
                "total" => $menu[0]->price * $value->qty,
            );
        }

        return view('orders.checkout', compact('order', 'details'));
    }

    public function genID()
    {
        $last = Order::where('status', 0)->count();
        $next = str_pad($last + 1, 3, "0", STR_PAD_LEFT);;
        $date = date('dmY');
        $format = "ABC" . $date . "-" . $next;

        return $format;
    }
    
    public function proceedCheckout(Request $request, $id)
    {
        $order = Order::find($id);
        $details = OrderDetail::where('order_id', $order->id)->get();
        $total = 0;
        foreach ($details as $detail) {
            $total += $detail->menus->price * $detail->qty;
        }

        $order->bill_number = $this->genID();
        $order->total_bill = $total;
        $order->status = 0;
        $order->check_out_date = date('Y-m-d H:i:s');
        $order->save();

        return redirect('/home/orders')->with('success', 'Order has been updated.');
    }

    public function printOrder($id)
    {
        $order = Order::find($id);
        $detail = OrderDetail::where('order_id', $id)->get();
        $details = [];
        foreach ($detail as $key => $value) {
            $menu = Menu::where('id', $value->menu_id)->get();
            $details[$key] = array(
                "id" => $value->id,
                "menu" => $menu[0]->name,
                "type" => $menu[0]->type,
                "qty" => $value->qty,
                "total" => $menu[0]->price * $value->qty,
            );
        }

        $pdf = PDF::loadView('orders.print', compact('order', 'details'))->setPaper('A4', 'potrait');
        return $pdf->stream();
    }
}
