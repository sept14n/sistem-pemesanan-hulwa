<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::group(['middleware' => ['web']], function () {
    Route::group(['middleware' => ['auth'] ], function () {

        Route::get('/home', 'HomeController@index')->name('home');

        // Route::resource('/home/users', 'UserController');
        Route::get('/home/users/getIndex', 'UserController@getIndex');
        Route::get('/home/users', 'UserController@index')->name('users.index');
        Route::get('/home/users/create', 'UserController@create')->name('users.create');
        Route::post('/home/users', 'UserController@store')->name('users.store');
        Route::get('/home/users/{id}', 'UserController@show')->name('users.show');
        Route::get('/home/users/{id}/edit', 'UserController@edit')->name('users.edit');
        Route::match(['put', 'patch'], '/home/users/{id}', 'UserController@update')->name('users.update');
        Route::get('/home/users/{id}/delete', 'UserController@destroy')->name('users.destroy');
        
        Route::get('/home/tables/getIndex', 'TableController@getIndex');
        Route::get('/home/tables', 'TableController@index')->name('tables.index');
        Route::get('/home/tables/create', 'TableController@create')->name('tables.create');
        Route::post('/home/tables', 'TableController@store')->name('tables.store');
        Route::get('/home/tables/{id}', 'TableController@show')->name('tables.show');
        Route::get('/home/tables/{id}/edit', 'TableController@edit')->name('tables.edit');
        Route::match(['put', 'patch'], '/home/tables/{id}', 'TableController@update')->name('tables.update');
        Route::get('/home/tables/{id}/delete', 'TableController@destroy')->name('tables.destroy');
        
        Route::get('/home/menus/getIndex', 'MenuController@getIndex');
        Route::get('/home/menus', 'MenuController@index')->name('menus.index');
        Route::get('/home/menus/create', 'MenuController@create')->name('menus.create');
        Route::post('/home/menus', 'MenuController@store')->name('menus.store');
        Route::get('/home/menus/{id}', 'MenuController@show')->name('menus.show');
        Route::get('/home/menus/{id}/edit', 'MenuController@edit')->name('menus.edit');
        Route::match(['put', 'patch'], '/home/menus/{id}', 'MenuController@update')->name('menus.update');
        Route::get('/home/menus/{id}/delete', 'MenuController@destroy')->name('menus.destroy');
        
        Route::get('/home/orders/getTable', 'OrderController@getTable')->name('orders.getTable');
        Route::get('/home/orders/getMenu', 'OrderController@getMenu')->name('orders.getMenu');
        Route::get('/home/orders', 'OrderController@index')->name('orders.index');
        Route::get('/home/orders/create', 'OrderController@create')->name('orders.create');
        Route::post('/home/orders', 'OrderController@store')->name('orders.store');
        Route::get('/home/orders/{id}', 'OrderController@show')->name('orders.show');
        Route::get('/home/orders/{id}/edit', 'OrderController@edit')->name('orders.edit');
        Route::match(['put', 'patch'], '/home/orders/{id}', 'OrderController@update')->name('orders.update');
        Route::get('/home/orders/{id}/delete', 'OrderController@destroy')->name('orders.destroy');

        Route::get('/home/orders/{id}/addItem', 'OrderController@createDetail')->name('orders.createDetail');
        Route::post('/home/orders/{id}/storeItem', 'OrderController@storeDetail')->name('orders.storeDetail');
        Route::get('/home/orders/{id}/deleteItem/{idDetail}', 'OrderController@deleteDetail')->name('orders.deleteDetail');
        Route::get('/home/orders/{id}/checkout', 'OrderController@checkout')->name('orders.checkout');
        Route::match(['put', 'patch'], '/home/orders/{id}/proceedCheckout', 'OrderController@proceedCheckout')->name('orders.proceedCheckout');
        Route::get('/home/orders/{id}/printOrder', 'OrderController@printOrder')->name('orders.printOrder');

        Route::get('/home/reports/getOrder', 'ReportController@getOrder')->name('reports.getOrder');
        Route::get('/home/reports/order', 'ReportController@order')->name('reports.order');
        Route::get('/home/reports/printOrder', 'ReportController@printOrder')->name('reports.printOrder');

    });
});